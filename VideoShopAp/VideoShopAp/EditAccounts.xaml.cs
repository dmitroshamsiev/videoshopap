﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace VideoShopAp
{
    /// <summary>
    /// Interaction logic for EditAccounts.xaml
    /// </summary>
    public partial class EditAccounts : Window
    {
        public Membership Account { get; set; }
        public Role Role { get; set; }

        public EditAccounts( Membership account, ListCollectionView employees, ListCollectionView roles)
        {
            InitializeComponent();
            Account = account;
            Password.Password = account.Password;
            Employees.ItemsSource = employees;
            Roles.ItemsSource = roles;

            DataContext = this;
        }

        private void Button_Click( object sender, RoutedEventArgs e )
        {
            Account.Password = Password.Password;
            Role = Roles.SelectedItem as Role;
            this.Close();
        }
    }
}
