﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace VideoShopAp
{
    /// <summary>
    /// Interaction logic for EditMovie.xaml
    /// </summary>
    public partial class EditMovie : Window
    {
        /// <summary>
        /// Edited movie
        /// </summary>
        public Movie Movie { get; set; }
        /// <summary>
        /// Initializes a new instance of the EditMovie class.
        /// </summary>
        /// <param name="movie">Movie to edit.</param>
        /// <param name="categories">All available categories</param>
        /// <param name="genres">All available genres</param>
        /// <param name="formats">All available formats</param>
        public EditMovie( Movie movie, ListCollectionView categories, ListCollectionView genres, ListCollectionView formats )
        {
            InitializeComponent();
            Movie = movie;
            Genres.ItemsSource = genres;
            Categories.ItemsSource = categories;
            Formats.ItemsSource = formats;

            DataContext = this;
        }
    }
}
