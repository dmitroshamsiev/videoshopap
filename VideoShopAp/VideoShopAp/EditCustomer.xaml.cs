﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace VideoShopAp
{
    /// <summary>
    /// Interaction logic for EditCustomer.xaml
    /// </summary>
    public partial class EditCustomer : Window
    {
        public Customer Customer { get; set; }
        public bool IsCancelled { get; set; }
        public EditCustomer(Customer customer)
        {
            InitializeComponent();
            Customer = customer;
            DataContext = this;
        }

        private void Button_Click( object sender, RoutedEventArgs e )
        {
            this.Close();
        }

        private void CancelCreation(object sender, RoutedEventArgs e)
        {
            IsCancelled = true;
            this.Close();
        }
    }
}
