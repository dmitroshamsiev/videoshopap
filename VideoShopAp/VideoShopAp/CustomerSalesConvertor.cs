﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;

namespace VideoShopAp
{
    public class CustomerSalesConvertor
        : IValueConverter
    {
        public object Convert( object value, Type targetType, object parameter, System.Globalization.CultureInfo culture )
        {
            var sales = value as ICollection<Sale>;
            var total = sales.Sum( s => s.Movie.SalesPrice * s.Quantity );
            return String.Format( culture, "{0:C}", total );
        }

        public object ConvertBack( object value, Type targetType, object parameter, System.Globalization.CultureInfo culture )
        {
            throw new NotImplementedException();
        }
    }
}
